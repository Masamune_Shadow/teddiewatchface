#include <TeddieText.h>

static Window *window;
BitmapLayer*	lyrWatchface;
GBitmap* 		imgBackground;
BitmapLayer*	lyrDialogBox;
GBitmap* 		imgDialogBox;
BitmapLayer*	lyrTeddie;
GBitmap* 		imgTeddie;

BitmapLayer*	lyrWhiteOutline;
GBitmap* 		imgWhiteOutline;

TextLayer*		txtlyrTimeLine1;
TextLayer*		txtlyrTimeLine2;
TextLayer*		txtlyrTimeLine3;

//int iCurrentTeddie;
int iCurrentHour;
int iPreviousHour;

//GFont fontDate; 
//ResHandle resDate;	

GFont fontTime; 
ResHandle resTime;	
GRect fraTimeFrameLine1;
GRect fraTimeFrameLine2;
GRect fraTimeFrameLine3;

#define TIMEFRAME_LINE1 (GRect(7,10,120,168))

P4FDATE P4FDate;
P4FTEXT P4FText;

static uint32_t TEDDIE_IMAGES[] = 
{
	RESOURCE_ID_IMAGE_TEDDIE_NORMAL,
	RESOURCE_ID_IMAGE_TEDDIE_BLUSHING,
	RESOURCE_ID_IMAGE_TEDDIE_HAPPY,
	RESOURCE_ID_IMAGE_TEDDIE_MAD,
	RESOURCE_ID_IMAGE_TEDDIE_NERVOUS,
	RESOURCE_ID_IMAGE_TEDDIE_SAD,
	RESOURCE_ID_IMAGE_TEDDIE_SURPRISED,
	RESOURCE_ID_IMAGE_TEDDIE_SHADOW
};


static void change_teddie()
{
	P4FText.iCurrentTeddie = rand() % 8;
	P4FText.iCurrentChoice = rand() % 3;
	
	//create the BG and attach it
	/*
	gbitmap_destroy(imgTeddie);
	imgTeddie = gbitmap_create_with_resource(TEDDIE_IMAGES[P4FText.iCurrentTeddie]);
	bitmap_layer_set_bitmap(lyrTeddie, imgTeddie);
	layer_mark_dirty(bitmap_layer_get_layer(lyrTeddie));
	*/
	
	layer_remove_from_parent(bitmap_layer_get_layer(lyrTeddie));
	bitmap_layer_destroy(lyrTeddie);
	gbitmap_destroy(imgTeddie);
	
	imgTeddie = gbitmap_create_with_resource(TEDDIE_IMAGES[P4FText.iCurrentTeddie]);
	lyrTeddie = bitmap_layer_create(GRect(17,-10,144,168));
	bitmap_layer_set_background_color(lyrTeddie,GColorClear);
	bitmap_layer_set_compositing_mode(lyrTeddie, GCompOpSet);	
	bitmap_layer_set_bitmap(lyrTeddie, imgTeddie);
	//layer_add_child(window_get_root_layer(window), bitmap_layer_get_layer(lyrTeddie));	
	//layer_insert_above_sibling(bitmap_layer_get_layer(lyrWhiteOutline), bitmap_layer_get_layer(lyrTeddie));	
	layer_add_child(bitmap_layer_get_layer(lyrWhiteOutline), bitmap_layer_get_layer(lyrTeddie));	
	layer_mark_dirty(bitmap_layer_get_layer(lyrTeddie));
	
	
	
	//update the text stuff.
}


static void window_load(Window *window) 
{
	//default the bear
	P4FText.iCurrentTeddie = 0;
	iPreviousHour = 88;
	iCurrentHour = 99;
	//setup the frames
	fraTimeFrameLine1 = TIMEFRAME_LINE1;

	//create the BG and attach it
	imgBackground = gbitmap_create_with_resource(RESOURCE_ID_IMAGE_BACKGROUND);
	lyrWatchface = bitmap_layer_create(GRect(0,0,144,168));
	bitmap_layer_set_background_color(lyrWatchface,GColorClear);
	bitmap_layer_set_compositing_mode(lyrWatchface, GCompOpSet);	
	bitmap_layer_set_bitmap(lyrWatchface, imgBackground);
	layer_add_child(window_get_root_layer(window), bitmap_layer_get_layer(lyrWatchface));	
	layer_mark_dirty(bitmap_layer_get_layer(lyrWatchface));
	
	P4FDATE_INIT(&P4FDate, bitmap_layer_get_layer(lyrWatchface));	
	
	imgWhiteOutline = gbitmap_create_with_resource(RESOURCE_ID_IMAGE_WHITEOUTLINE);
	lyrWhiteOutline = bitmap_layer_create(GRect(0,0,144,168));
	bitmap_layer_set_background_color(lyrWhiteOutline,GColorClear);
	bitmap_layer_set_compositing_mode(lyrWhiteOutline, GCompOpSet);	
	bitmap_layer_set_bitmap(lyrWhiteOutline, imgWhiteOutline);
	layer_add_child(window_get_root_layer(window), bitmap_layer_get_layer(lyrWhiteOutline));	
	layer_mark_dirty(bitmap_layer_get_layer(lyrWhiteOutline));
	
	P4FText.iCurrentTeddie = rand() % 8;
	P4FText.iCurrentChoice = rand() % 3;
	//create the BG and attach it
	
	imgTeddie = gbitmap_create_with_resource(TEDDIE_IMAGES[P4FText.iCurrentTeddie]);
	lyrTeddie = bitmap_layer_create(GRect(17,-10,144,168));
	bitmap_layer_set_background_color(lyrTeddie,GColorClear);
	bitmap_layer_set_compositing_mode(lyrTeddie, GCompOpSet);	
	bitmap_layer_set_bitmap(lyrTeddie, imgTeddie);
	layer_add_child(window_get_root_layer(window), bitmap_layer_get_layer(lyrTeddie));	
	layer_mark_dirty(bitmap_layer_get_layer(lyrTeddie));
	
	//load the text dialog box
	imgDialogBox = gbitmap_create_with_resource(RESOURCE_ID_IMAGE_DIALOG);
	lyrDialogBox = bitmap_layer_create(GRect(0,112,139,56));
	bitmap_layer_set_background_color(lyrDialogBox,GColorClear);
	bitmap_layer_set_compositing_mode(lyrDialogBox, GCompOpSet);	
	bitmap_layer_set_bitmap(lyrDialogBox, imgDialogBox);
	layer_add_child(window_get_root_layer(window), bitmap_layer_get_layer(lyrDialogBox));	
	layer_mark_dirty(bitmap_layer_get_layer(lyrDialogBox));
	
	//load the custom font for the time
	resTime = resource_get_handle(RESOURCE_ID_FONT_DAYS_14);
	fontTime = fonts_load_custom_font(resTime);
	
	//load the date layer
	time_t tTime = time(NULL);
	struct tm* tmTime = localtime(&tTime);
	iCurrentHour = tmTime->tm_hour;
	iPreviousHour = iCurrentHour;
	
	
	//create the text layer, put it on top of the watchface layer.
	txtlyrTimeLine1 = text_layer_create(fraTimeFrameLine1);
	text_layer_set_font(txtlyrTimeLine1, fontTime);
	text_layer_set_text_color(txtlyrTimeLine1, GColorWhite);
	text_layer_set_background_color(txtlyrTimeLine1,GColorClear);
	text_layer_set_text(txtlyrTimeLine1, P4FText.Line1);
	text_layer_set_text_alignment(txtlyrTimeLine1, GTextAlignmentLeft);
	text_layer_set_overflow_mode(txtlyrTimeLine1, GTextOverflowModeWordWrap);
	layer_add_child(bitmap_layer_get_layer(lyrDialogBox), text_layer_get_layer(txtlyrTimeLine1)); 
	layer_mark_dirty(text_layer_get_layer(txtlyrTimeLine1));
	
	//this should have given it enough time to startup
	SetCurrentDateAndDateWord(&P4FDate,tmTime,bitmap_layer_get_layer(lyrWatchface));
	
}

static void handle_minute_tick(struct tm* t , TimeUnits units_changed) 
{

	  if (iCurrentHour != t->tm_hour)
	 {
		 iCurrentHour = t->tm_hour;
	 }
	 
	 if (P4FDate.iPreviousDay != t->tm_mday)
	 {
		SetCurrentDateAndDateWord(&P4FDate,t,bitmap_layer_get_layer(lyrWatchface));
	 }
	 

	 if (iPreviousHour != iCurrentHour)
	 {
		 iPreviousHour = iCurrentHour;
		 change_teddie();
	 }
	 
	 
	 UpdateTime(&P4FText, t);
	
	text_layer_set_text(txtlyrTimeLine1, P4FText.Line1);
	layer_mark_dirty(text_layer_get_layer(txtlyrTimeLine1));
	
}


static void init(void) {
  window = window_create();
  window_set_background_color(window, GColorBlack);
  window_set_window_handlers(window, (WindowHandlers) {
    .load = window_load,

  });
  tick_timer_service_subscribe(MINUTE_UNIT, handle_minute_tick);
  
  window_stack_push(window, true);
}

static void deinit(void) {
	layer_remove_from_parent(bitmap_layer_get_layer(lyrWatchface));
	bitmap_layer_destroy(lyrWatchface);
	gbitmap_destroy(imgBackground);
	layer_remove_from_parent(bitmap_layer_get_layer(lyrDialogBox));
	bitmap_layer_destroy(lyrDialogBox);
	gbitmap_destroy(imgDialogBox);
	layer_remove_from_parent(bitmap_layer_get_layer(lyrTeddie));
	bitmap_layer_destroy(lyrTeddie);
	gbitmap_destroy(imgTeddie);

	layer_remove_from_parent(bitmap_layer_get_layer(lyrWhiteOutline));
	bitmap_layer_destroy(lyrWhiteOutline);
	gbitmap_destroy(imgWhiteOutline);
	
	
	layer_remove_from_parent(text_layer_get_layer(txtlyrTimeLine1));
	text_layer_destroy(txtlyrTimeLine1);

	fonts_unload_custom_font(fontTime);	
	
	RemoveAndDeIntAllDate(&P4FDate);
	
	window_destroy(window);
}

int main(void) {
  init();
  app_event_loop();
  deinit();
}
